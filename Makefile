CC=clang
CFLAGS= -Wall

default: main.o process.o nn.o
		$(CC) process.o nn.o main.o -o procon
		
main.o: main.c
		$(CC) $(CFLAGS) -c main.c

process.o: process.c
		$(CC) $(CFLAGS) -c process.c
		
nn.o: nn.c
		$(CC) $(CFLAGS) -c nn.c
clean:
	rm *.o procon